# Datos

* Formación de 7 jugadores
* Sabemos: Nombre del jugador, Posición en la que juega y calificación a como
  juega en esa posición
* Se busca la mejor formación (mayor calificación)



# Integrantes del club

* Los integrantes están en un archivo de texto con los 3 campos de información
* No hay 2 personas con el mismo nombre, si hay entradas en el archivo de texto
  con el mismo nombre, entonces significa que esa persona puede jugar en varias
  posiciones.
* Son 3 posiciones Atacante (A), medio campista (M) o defensa (D)
* La calificación del jugador va de 1 a 10
* Los clubs tienen normas de como formar equipos: mínimo y máximo de jugadores
que puede ser asignados a una posición con la condición que el total sean 6
jugadores, menos el arquero que no puede ser reemplazado (total 7 jugadores)

- Rango atacantes: 1 a 3
- Rando defensas: 2 a 3
- Rando medio campistas: 1 a 3


# Criterio de optimalidad

* Asegurar que exista la minima cantidad de jugadores necesarios para completar
el equipo.
* Para hacer esto se debe revisar todos los inscritos y comprobar la cantidad de
jugadores por posiciones disponibles, es decir, ver cuantos jugadores hay
disponibles por cada posición.
* Si hay jugadores que están en más de una posición, se debe ir a la posición
que tenga menos jugadores (independiente de la calificación que tenga)
* Se debe cumplir, después de tener las nominas de los jugadores, con:
        CantMaxDefensas + CantidadMaximaAtacantes + CantidadMaximaMedioCampista >= 6
        CantMaximaA = Min{CanTotal de jugadores en A}
  En caso de no cumplirse, no existe solución para la heurística ni para la
  fuerza bruta.


# Solucion

## Enumeracion Explicita

* El equipo debe tener 6 jugadores
* Ser la formación con mayor puntaje
* No deben haber nombres repetidos en la formación
* Cumpla con los rangos  en las posiciones mencionadas antes


## Heuristica

* Se obtiene la cantidad máxima por posición (formula 2.3.1)
* Calcular el promedio con la formula de cada persona
* Luego me mayor a menor promedio calcular la cantidad de jugadores por cada
  posición


# Requerimientos del problema

## Entrada

* -f ARCHIVO DE ENTRADA
* Parte con un numero n (cantidad de jugadores)
* Los datos son separados por un espacio


## Salida

* Debe contener un header
* [Heurística]
* [Enumeración Explicita]
