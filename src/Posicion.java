/**Una clase enum que representa las posibles posiciones que puede tener un
 * jugador*/
enum Posicion{
        ATACANTE, DEFENSA, CAMPISTA;


        @Override
        public String toString(){

                switch(this)
                {
                        case ATACANTE:
                                return "Atacante";
                        case DEFENSA:
                                return "Defensa";
                        case CAMPISTA:
                                return "Medio Campista";
                        default:
                                return "No hay string para mostrar la posicion";
                }

        }

}



/**Excepcion lanzada cuando la posicion de un jugador no es valida*/
class PosicionNoValida extends Exception{


}
