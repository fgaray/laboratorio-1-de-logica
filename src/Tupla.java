

/**Implementa una tupla inmutable*/
public class Tupla<T1, T2>{

        private final T1 t1;
        private final T2 t2;

        /**Crea una tupla con los elementos t1 y t2*/
        Tupla(final T1 t1, final T2 t2)
        {
                this.t1 = t1;
                this.t2 = t2;
        }


        /**Devuelve el primer elemento de la tupla*/
        public T1 fst()
        {
                return this.t1;
        }


        /**Devuelve el segundo elemento de la tupla*/
        public T2 snd()
        {
                return this.t2;
        }


        @Override
        public String toString()
        {
                return "(" + this.t1 + ", " + this.t2 + ")";
        }

}
