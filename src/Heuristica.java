import java.util.*;


public class Heuristica{


        /**funcion que calcula la mejor formacion usando la heuristica planteada
         * en el enunciado
         *
         * @param js Todos los jugadores disponibles para hacer la formacion
         *
         * @return La mejor formacion dada por formacion
         * */
        static Formacion resolver(final ArrayList<Jugador> js)
        {

                ArrayList<Jugador> filtrados = Jugador.criterioOptimalidad(js);


                //calculamos cada promedio de cada posicion
                double promedio_atacantes = Jugador.promedioAtacantes(js);
                double promedio_defensas = Jugador.promedioDefensas(js);
                double promedio_campistas = Jugador.promedioCampistas(js);

                
                ArrayList<Tupla<Double, Posicion>> promedios = new ArrayList<Tupla<Double, Posicion>>();

                //guaramos los promedios en una array para poder ordenarlos
                promedios.add(new Tupla<Double, Posicion>(promedio_atacantes, Posicion.ATACANTE));
                promedios.add(new Tupla<Double, Posicion>(promedio_defensas, Posicion.DEFENSA));
                promedios.add(new Tupla<Double, Posicion>(promedio_campistas, Posicion.CAMPISTA));



                ArrayList<Tupla<Integer, Posicion>> optimas = ajustarOptimas(calcularOptimas(js, promedios));


                Formacion formacion = new Formacion();


                for(Tupla<Integer, Posicion> t: optimas)
                {
                        ArrayList<Jugador> j = null;

                        if(t.snd() == Posicion.ATACANTE)
                        {
                                j = Jugador.atacantes(filtrados);

                        }else if(t.snd() == Posicion.DEFENSA){
                                
                                j = Jugador.defensas(filtrados);

                        }else if(t.snd() == Posicion.CAMPISTA){
                                j = Jugador.campistas(filtrados);
                        }

                        if(j != null)
                        {
                                Collections.sort(j, new ComparadorPuntajes());

                                for(int i = 0; i < t.fst() && i < j.size(); i++)
                                {
                                        formacion.setJugador(j.get(i));
                                }

                        }
                }


                return formacion;
        }



        /**Calcula las cantidades optimas de  jugadores que deben ir en la
         * posicion
         *
         * @param cantidad_max La cantidad maxima de jugadores que pueden ir en
         * una posicion
         *
         * @param jugadores_disponibles El total de jugadores disponibles al
         * momento de ir agregando jugadores
         *
         * @return La cantidad optima de jugadores
         * */
        private static int cantidad_optima(int cantidad_max, int jugadores_disponibles)
        {
                return Math.min(cantidad_max, jugadores_disponibles);
        }


        /**Calcula las cantidades optimas de jugadores a partir de los promedios
         * dados, se le da preferencia a los mayores promedios
         *
         * @param js La lista de todos los jugadores disponibles
         *
         * @param promedios Una tupla de <Promedio, Posicion> de una posicion
         * para saber en que orden calcular las cantidades optimas
         *
         * @return Un array con una tupla de la cantidad optima de jugadores que
         * deben ir en la posicion en el segundo elemento de la tupla
         *
         * */
        private static ArrayList<Tupla<Integer, Posicion>> calcularOptimas(final ArrayList<Jugador> js, final ArrayList<Tupla<Double, Posicion>> promedios)
        {

                Collections.sort(promedios, new ComparadorTupla());

                ArrayList<Tupla<Integer, Posicion>> optimas = new ArrayList<Tupla<Integer, Posicion>>();

                int max_atacantes = Jugador.maxAtacantes(js);
                int max_defensas = Jugador.maxDefensas(js);
                int max_campistas = Jugador.maxCampistas(js);


                int max_jugadores = 6;


                for(Tupla<Double, Posicion> t: promedios)
                {
                        //debemos obtener las cantidades obtimas para cada
                        //posicion


                        if(t.snd() == Posicion.ATACANTE)
                        {
                                int optima = cantidad_optima(max_atacantes, max_jugadores);

                                optimas.add(new Tupla<Integer, Posicion>(optima, t.snd()));

                                max_jugadores = max_jugadores - optima;

                        }else if(t.snd() == Posicion.DEFENSA){

                                int optima = cantidad_optima(max_defensas, max_jugadores);

                                optimas.add(new Tupla<Integer, Posicion>(optima, t.snd()));

                                max_jugadores = max_jugadores - optima;

                        }else if(t.snd() == Posicion.CAMPISTA){

                                int optima = cantidad_optima(max_campistas, max_jugadores);

                                optimas.add(new Tupla<Integer, Posicion>(optima, t.snd()));

                                max_jugadores = max_jugadores - optima;
                        }
                }

                return optimas;
        }


        /**Ajusta la cantidad de jugadores que deben ir en una posicion para que
         * no exista ninguna que no tenga cero jugadores
         *
         * @param optimas Una lista de tuplas que representan las cantidades de
         * jugadores en cada posicion
         *
         * @return Una lista con las cantiadades arregladas si es que existe una
         * posicion sin jugadores.
         *
         * */
        static ArrayList<Tupla<Integer, Posicion>> ajustarOptimas(final ArrayList<Tupla<Integer, Posicion>> optimas)
        {
                for(int i = 1; i < optimas.size(); i++)
                {
                        if(optimas.get(i).fst() == 0)
                        {
                                Tupla<Integer, Posicion> anterior, actual;

                                actual = optimas.remove(i);
                                anterior = optimas.remove(i - 1);

                                optimas.add(new Tupla<Integer, Posicion>(anterior.fst() - 1, anterior.snd()));
                                optimas.add(new Tupla<Integer, Posicion>(actual.fst() + 1, actual.snd()));
                        }
                }

                return optimas;
        }

}


/**Comparador de las tuplas, ordena de forma inversa*/
class ComparadorTupla implements Comparator<Tupla<Double, Posicion>> {
        @Override
        public int compare(Tupla<Double, Posicion> o1, Tupla<Double, Posicion> o2) {
                return Double.compare(o2.fst(), o1.fst());
        }
}


/**Comparador de puntajes inverso*/
class ComparadorPuntajes implements Comparator<Jugador> {
        @Override
        public int compare(Jugador o1, Jugador o2) {
                return Double.compare(o2.getPuntaje(), o1.getPuntaje());
        }
}


