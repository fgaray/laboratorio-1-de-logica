import java.io.*;
import java.util.*;


/**A partir de un archivo*/
public class Parser{

        private final BufferedReader buffer;
        private ArrayList<Jugador> js;


        Parser(String archivo)throws FileNotFoundException
        {
                this.buffer = new BufferedReader(new FileReader(archivo));
                this.js = null;
        }

        Parser(BufferedReader buff)
        {
                this.buffer = buff;
                this.js = null;
        }


        /**Crea un arraylist de jugadores a partir de BufferedReader buffer de
         * la clase
         *
         * @return Un ArrayList de jugadores*/
        public void parseFile()throws IOException, PosicionNoValida
        {
                String line;
                ArrayList<Jugador> array = new ArrayList<Jugador>();


                this.buffer.readLine(); //nos saltamos la primera linea


                while((line = this.buffer.readLine()) != null) {
                        if(!line.equals("0"))
                        {
                                String[] tokens = line.split("\\ ");
                                array.add(new Jugador(tokens[0], tokens[1], tokens[2]));
                        }
                }

                this.js = array;
        }


        /**Devuelve los jugadores que hay en el archivo
         *
         * @return Un ArrayList de jugadores*/
        public ArrayList<Jugador> getJugadores()
        {
                return this.js;
        }


        /**Verifica si es posible dar solucion con los jugadores que hay en el
         * archivo
         *
         * @return true si se puede dar una solucion, false si no se puede dar
         * una solucion
         * */
        public boolean existeSolucion()
        {
                return Jugador.maxAtacantes(this.js) + Jugador.maxDefensas(this.js) + Jugador.maxCampistas(this.js) >= 6;
        }
}
