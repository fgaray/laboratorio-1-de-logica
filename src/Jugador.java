import java.util.*;


/**Clase que representa a un jugador con sus caracteristicas*/
public class Jugador{


        private final String nombre;
        private final Posicion posicion;
        private final int puntaje;

	
        Jugador(String nombre, Posicion posicion, int puntaje)
        {
                this.nombre = nombre;
                this.posicion = posicion;
                
                if(puntaje < 1 && puntaje > 10)
                {
                        this.puntaje = puntaje;
                }else{
                        this.puntaje = puntaje;
                }
        }


        Jugador(String nombre, String posicion, int puntaje)throws PosicionNoValida
        {
                Posicion pos;

                if(posicion.equalsIgnoreCase("A"))
                {
                        pos = Posicion.ATACANTE;
                }else if(posicion.equalsIgnoreCase("M")){
                        pos = Posicion.CAMPISTA;
                }else if(posicion.equalsIgnoreCase("D")){
                        pos = Posicion.DEFENSA;
                }else{
                        throw new PosicionNoValida();
                }

                this.nombre = nombre;
                this.posicion = pos;
                this.puntaje = puntaje;
        }


        Jugador(String nombre, String posicion, String puntaje)throws PosicionNoValida
        {
                this(nombre, posicion, Integer.parseInt(puntaje));
        }



        /**Retorna el nombre del jugador*/
        String getNombre()
        {
                return this.nombre;
        }

        /**Retorna el puntaje del jugador*/
        int getPuntaje()
        {
                return this.puntaje;
        }


        /**Retorna la posicion del jugador*/
        Posicion getPosicion()
        {
                return this.posicion;
        }


        /**Implementacion del metodo toString
         *
         * @return Un string representando a este objeto*/
        @Override
        public String toString()
        {
                return "" + this.nombre + "\t" + this.puntaje + "\t" + this.posicion;
        }



        /** Obtiene los jugadores atacantes en el arraylist
         *
         * @param js El array en el cual buscar los atacantes
         *
         * @return Un ArrayList de jugadores atacantes
         * */
        static ArrayList<Jugador> atacantes(final ArrayList<Jugador> js)
        {
		ArrayList<Jugador> njs = new ArrayList<Jugador>();
		
		for(Jugador j: js)	
		{			
			if(j.getPosicion() == Posicion.ATACANTE)
			{
				njs.add(j);
			}
		}

		return njs;
        }

        /**Obtiene la cantidad de jugadores atacantes
         *
         * @param js El array en el cual se buscan los atacantes
         *
         * @return La cantidad de atacantes encontrados
         * */
        static int cantidadAtacantes(final ArrayList<Jugador> js)
        {
                return atacantes(js).size();
        }


        /** Obtiene los jugadores campistas en el arraylist
         *
         * @param js El array en el cual buscar los atacantes
         *
         * @return Un ArrayList de jugadores campistas
         * */
        static ArrayList<Jugador> campistas(final ArrayList<Jugador> js)
        {
		ArrayList<Jugador> njs = new ArrayList<Jugador>();
				
		for(Jugador j: js)
		{
			if(j.getPosicion() == Posicion.CAMPISTA)
			{
				njs.add(j);
			}
		}

		return njs;
        }


        /**Obtiene la cantidad de jugadores campistas
         *
         * @param js El array en el cual se buscan los campistas
         *
         * @return La cantidad de campistas encontrados
         * */
        static int cantidadCampistas(final ArrayList<Jugador> js)
        {
                return campistas(js).size();
        }
        

        /** Obtiene los jugadores defenas en el arraylist
         *
         * @param js El array en el cual buscar los atacantes
         *
         * @return Un ArrayList de jugadores defensas
         * */
        static ArrayList<Jugador> defensas(final ArrayList<Jugador> js)
        {
                ArrayList<Jugador> njs = new ArrayList<Jugador>();
		
		for(Jugador j: js)
		{
			if(j.getPosicion() == Posicion.DEFENSA)
			{
				njs.add(j);
			}			
			
		}

		return njs;
        }


        /**Obtiene la cantidad de jugadores defensas
         *
         * @param js El array en el cual se buscan los defensas
         *
         * @return La cantidad de defensas encontrados
         * */
        static int cantidadDefensas(final ArrayList<Jugador> js)
        {
                return defensas(js).size();
        }


        /**
         * @return El maximo de atacantes disponibles*/
        static int maxAtacantes(final ArrayList<Jugador> js)
        {
                 return Math.min(Jugador.cantidadAtacantes(js), 3);
        }


        /**
         * @return El maximo de defensas disponibles*/
        static int maxDefensas(final ArrayList<Jugador> js)
        {
                return Math.min(Jugador.cantidadDefensas(js), 3);
        }


        /**
         * @return El maximo de campistas disponibles*/
        static int maxCampistas(final ArrayList<Jugador> js)
        {
                return Math.min(Jugador.cantidadCampistas(js), 3);
        }


        /**Calcula el promedio de los jugadores atacantes
         *
         * @param js El array en el cual se van a buscar los jugadores
         * atacantes
         *
         * @return El promedio de los jugadores atacantes
         * */
        static double promedioAtacantes(final ArrayList<Jugador> js)
        {
                return promedio(Jugador.atacantes(js));
        }


        /**Calcula el promedio de los jugadores defensas
         *
         * @param js El array en el cual se van a buscar los jugadores
         * defensas
         *
         * @return El promedio de los jugadores defensas
         * */
        static double promedioDefensas(final ArrayList<Jugador> js)
        {
                return promedio(Jugador.defensas(js));
        }



        /**Calcula el promedio de los jugadores medio campistas
         *
         * @param js El array en el cual se van a buscar los jugadores
         * campistas
         *
         * @return El promedio de los jugadores medio campistas
         * */
        static double promedioCampistas(final ArrayList<Jugador> js)
        {
                return promedio(Jugador.campistas(js));
        }




        /**Calcula el promedio de con los jugadores dados
         *
         * @return El promedo de los jugadores dados*/
        private static double promedio(final ArrayList<Jugador> js)
        {
                double sum = 0;

                for(Jugador j: js)
                {
                        sum += j.getPuntaje();
                }

                return 1.0 / js.size() * sum;
        }


        /**Aplica el criterio de optimalidad al array dado
         *
         * @param js El array al cual se va a aplicar el criterio de optimalidad
         *
         * @return Un array de jugadores con el criterio de optimalidad aplicado 
         * */
        public static ArrayList<Jugador> criterioOptimalidad(final ArrayList<Jugador> js)
        {
                ArrayList< ArrayList<Jugador> > jugadores = new ArrayList< ArrayList<Jugador> >();

                jugadores.add(Jugador.campistas(js));
                jugadores.add(Jugador.defensas(js));
                jugadores.add(Jugador.atacantes(js));

                Collections.sort(jugadores, new ComparadorJugadores());

                for(int i = 0; i + 1 < jugadores.size(); i++)
                {
                       if(jugadores.get(i).size() > jugadores.get(i + 1).size())
                       {
                                //debemos buscar si hay un jugador repetido
                                //entre estas 2 listas, y dejar los jugadores
                                //repetidos en la lista con menos jugadores

                                for(Jugador j2: jugadores.get(i + 1))
                                {
                                        for(Jugador j1: jugadores.get(i))
                                        {
                                                if(j1.getNombre().equals(j2.getNombre()))
                                                {
                                                        borrar(jugadores.get(i), j1);
                                                        break;
                                                }
                                        }
                                }
                       }
                }

                //concatenamos los jugadores

                ArrayList<Jugador> filtrados = new ArrayList<Jugador>();

                for(ArrayList<Jugador> j: jugadores)
                {
                        filtrados.addAll(j);
                }


                return filtrados;

        }


        /**Borra el jugador dado del array
         *
         * @param js El array del cual se va a sacar el jugador
         * @param j El jugador que se va a eliminar del array*/
        private static void borrar(ArrayList<Jugador> js, Jugador j)
        {
                for(int i = 0; i < js.size(); i++)
                {
                        if(js.get(i).getNombre().equals(j.getNombre()))
                        {
                                js.remove(i);
                                i = -1;
                        }
                }
        }
}


/**Comparador de jugadores inverso*/
class ComparadorJugadores implements Comparator<ArrayList<Jugador>> {
        @Override
        public int compare(ArrayList<Jugador> o1, ArrayList<Jugador> o2) {
                return Double.compare(o2.size(), o1.size());
        }
}
