
import java.io.*;
import java.util.*;


public class main{


        /**Punto de entrada del programa
         * */
        public static void main (String[] args)
        {

                String nombre_archivo = parserArgumentos(args);

                if(nombre_archivo == null)
                {
                        System.out.println("Debe especificar un archivo a leer con la opcion -f");
                        System.exit(-1);
                }


                Parser p = null;

                try{
                         p = new Parser(nombre_archivo);

                }catch(FileNotFoundException e){
                        System.out.println("Archivo no encontrado");
                        System.exit(-1);
                }

                ArrayList<Jugador> js = null;

                try{
                         p.parseFile();
                         if(!p.existeSolucion())
                         {
                                System.out.println("No existe solucion para el archivo dado");
                                System.exit(-1);
                         }

                         js = p.getJugadores();

                }catch(PosicionNoValida e){
                        System.out.println("Posicion del jugador no valida");
                        System.exit(-1);
                }catch(IOException e){
                        System.out.println("Error leyendo el archivo");
                        System.exit(-1);
                }

                mostrarBanner();
		
                System.out.println("[Heurística]");
                System.out.println(Heuristica.resolver(js));
                System.out.println("[Enumeración Explicita]");
		System.out.println(FuerzaBruta.resolver(js));

        }


        /**Revisa los argumentos de entrada hasta encontrar el archivo a leer
         * 
         * @param args Un array con los argumentos recibidos por el programa
         *
         * @return El nombre del archivo dado en los argumentos
         * */
        private static String parserArgumentos(String[] args)
        {
                for(int i = 0; i < args.length; i++)
                {
                        if(args[i].equals("-f") && i + 1 < args.length)
                        {
                                return args[i + 1];
                        }
                }

                return null;
        }



        /**Muestra el banner con el titulo del laboratorio y los integrantes
         * */
        private static void mostrarBanner()
        {
                System.out.println("##############################################");
                System.out.println("#           Laboratorio 1 LTC                #");
                System.out.println("#       Diego Díaz   - Felipe Garay          #");
                System.out.println("##############################################");
        }


}
