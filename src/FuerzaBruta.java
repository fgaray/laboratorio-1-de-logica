import java.util.*;
import java.io.*;


public class FuerzaBruta{

	static int puntajeMax;


        /**Funcion que calcula la mejor formacion usando fuerza bruta
         *
         * @param js Todos los jugadores disponibles para hacer la formacion
         *
         * @return La mejor formacion dada por formacion
         * */
        static Formacion resolver(final ArrayList<Jugador> js)
        {
		puntajeMax=0;	
		int n=js.size();		
		int k=6;
		Formacion optima = new Formacion();
		Formacion temp = new Formacion();
		
		combinaciones(js,0,n,k,temp,optima);
		

                return optima;
        }


	/**Funcion que calcula las posibles combinaciones de 6 de la lista de jugadores
         *
         * @param lista De jugadores que posee todos los jugadores leidos desde el archivo
         * @param posicion Es el indice perteneciente a la lista de jugadores para saber que jugador agregar a la formacion
	 * @param n El numero de elementos totales de la lista de jugadores
         * @param k La tamaño del cunjuto que se realizara (para este caso las formaciones son de largo 6)
         * @param tempAlineacion Es la formacion que ayudara a generar cada combinacion de formaciones posibles
	 * @param optima es la formacion que quedará como optima y será la mejor
         *
         * */
	static void combinaciones(ArrayList<Jugador> lista,int posicion,int n,int k,Formacion tempAlineacion, Formacion optima)
	{

		int i;
   		
		/* Si la combinatoria es invalida */
		if(n < k)	
		{ 
			/* Es imposible calcular la combianctoria entre las formaciones posibles */
			System.out.println("No se pueden buscar las combinaciones");
        		
			return; /* Volvemos inmediatamente */
	    	}
	
    		else
		{	           
        		/* Ya se obtuvo una posible formacion */
		        if(k == 0)
			{
				/* Si la formacion generada es valida */
				if(tempAlineacion.valida()==true)
				{			
					/* Si se encuentra una formacion con mayor puntaje, reemplazarla como candidata en optima */
					if(puntajeMax<=tempAlineacion.getPuntajeTotal()) 
					{	
						/* El nuevo puntaje maximo es el ultimo puntaje mas alto de una formacion*/
						puntajeMax=tempAlineacion.getPuntajeTotal();	
						optima.eliminarFormacion(); /* Se elimina la formacion anterior candidata*/
							
						for(int j=0;j <tempAlineacion.sizeFormacion(); j++ )
						{	
							/* Ingresamos los jugadores de la formacion candidata*/
							optima.setJugador(tempAlineacion.getJugador(j)); 
						}		
					}
				}
				
				/* Removemos el ultimo jugador de la formacion para poder agregar uno siguiente y quedar con el mismo
				 * largo k */
				tempAlineacion.removeJugador((tempAlineacion.sizeFormacion()-1));			
				
				/* Ya generamos una posible formacion nos devolvemos para seguir generando */ 
				return;
        		}
			
			
			
			/* Iterativamente llamada a la funcion recursiva */
		        for(i=0 ; i <= n - k ; i++)
			{
				/* Ingresamos el jugador correspondiente a una combinacion a la formacion */
				tempAlineacion.setJugador(lista.get(i+posicion));
				
				combinaciones(lista,i+1+posicion,n-1-i,k-1,tempAlineacion,optima);
        		}
			
			/* Si la formacion no esta vacia */
			if(tempAlineacion.formacionVacia()==false)
			{        		
				/* Nuevamente removemos el ultimo jugador para comenzar las combinaciones del anterior anterior*/
				tempAlineacion.removeJugador((tempAlineacion.sizeFormacion()-1));

			}

	        }

    			return;
	}


}
