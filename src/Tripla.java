
/**Implementa una tripla inmutable*/
public class Tripla<T1, T2, T3>{

        private final T1 t1;
        private final T2 t2;
        private final T3 t3;

        /**Crea una tupla con los elementos t1 y t2*/
        Tripla(final T1 t1, final T2 t2, final T3 t3)
        {
                this.t1 = t1;
                this.t2 = t2;
                this.t3 = t3;
        }


        /**Devuelve el primer elemento de la tupla*/
        public T1 fst()
        {
                return this.t1;
        }


        /**Devuelve el segundo elemento de la tupla*/
        public T2 snd()
        {
                return this.t2;
        }


        public T3 thd()
        {
                return this.t3;
        }


        @Override
        public String toString()
        {
                return "(" + this.t1 + ", " + this.t2 + ", " + this.t3 + ")";
        }

}
