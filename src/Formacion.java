import java.util.*;

/** Clase que representa una formacion, con la lista de jugadores titulares **/
public class Formacion{

	private ArrayList<Jugador> titulares; /** Atributo, lista de jugadores titulares */


	/**Contrsuctor crea formacion vacia
         * */
	Formacion()
	{

		this.titulares = new ArrayList<Jugador>();

	} 

	/**Constructor que crea una formacion al instante
         * */
	Formacion(Jugador arquero, Jugador j1, Jugador j2, Jugador j3, Jugador j4, Jugador j5)
	{
		this.titulares = new ArrayList<Jugador>();

		this.titulares.add(arquero);
		this.titulares.add(j1);		
		this.titulares.add(j2);		
		this.titulares.add(j3);		
		this.titulares.add(j4);		
		this.titulares.add(j5);				
			
	}
	
	

        /**Constructor que crea una formacion a partir del arraylist de los
         * jugadores*/
        Formacion(ArrayList<Jugador> titulares)
        {
                this.titulares = titulares;
        }
	
	/**Funcion que retorna el jugador i de la formacion
         *
         * @param i Posicon del jugador en el arraylist de la formacion
         *
         * @return El jugador en la posicion
         * */
	Jugador getJugador(int i){
		
		return this.titulares.get(i);
	}

	/**Funcion que agrega un jugador a la Formacion
         *
         * @param j El jugador a agregar a la formacion
         *
         * */
	void setJugador(Jugador j){
	
		this.titulares.add(j);
	}


	/**Funcion que deja una formacion vacia
         *
         * */
	void eliminarFormacion()
	{
	
		this.titulares.clear();

	}

	
	/**Funcion que saca un jugador de la Formacion
         *
         * @param posicion La posicion donde se remueve el jugador
         *
         * */
	void removeJugador(int posicion)
	{
	
		this.titulares.remove(posicion);

	}	
	
	
	/**Funcion vecifica si una funcion esta vacia
         *
         * @return true Si esta vacia
         *
         * */
	boolean formacionVacia()
	{
		return this.titulares.isEmpty();
	}

	
	/**Funcion que calcula el tamaño de una formacion
         *
         * @return El tamaño de la formacion
         *
         * */
	int sizeFormacion()
	{
		
		return this.titulares.size();	
	}
	

	/** Metodo que calcula el numero de defensas en la formacion
	 *  	
	 *  @return El numero de defensas en una formacion
	 * */
	  
	int getDefensas()
	{
                return Jugador.cantidadDefensas(this.titulares);
	}
	
	/**Metodo que calcula el numero de Medio campistas en la formacion 
	 *  	
	 *  @return El numero de medio campistas en una formacion
	 * */
	int getCampistas()
	{
                return Jugador.cantidadCampistas(this.titulares);
	}	

	
	/**Metodo que calcula el numero de atacantes en la formacion 
	 *  	
	 *  @return El numero de atacantes en una formacion
	 * */
	int getAtacantes()
	{
                return Jugador.cantidadAtacantes(this.titulares);
	}


	/**Metodo que calcula el puntaje total de la formacion
	 *  	
	 *  @return El puntaje de la formacion
	 * */
	int getPuntajeTotal()
	{

		int puntaje = 0;		
		
		for(Jugador j: this.titulares)	
		{			

			puntaje += j.getPuntaje();
			
		}
		
		return puntaje;
	}

	
        /** Verifica si la formacion es valida o no
         *
         * @return true si la formacion contiene la cantidad adecuada de
         * jugadores y en las categorias dadas. false si no es valida la
         * formacion
         * */
        boolean valida()
        {
                int campistas = this.getCampistas();
                int atacantes = this.getAtacantes();
                int defensas = this.getDefensas();

                boolean valido_campistas = 1 <= campistas && campistas <= 3;
                boolean valido_atacantes = 1 <= atacantes && atacantes <= 3;
                boolean valido_defensas = 1 <= defensas && defensas <= 3;
                boolean valida_cantidad_total = 6 == this.titulares.size();


                return valido_campistas && valido_atacantes && valido_defensas && valida_cantidad_total && !this.jugadorRepetido();
        }


        private boolean jugadorRepetido()
        {
                for(int i = 0; i < this.titulares.size(); i++)
                {
                        for(int j = i + 1; j < this.titulares.size(); j++)
                        {
                                if(this.titulares.get(i).getNombre().equals(this.titulares.get(j).getNombre()))
                                {
                                        return true;
                                }
                        }
                }

                return false;
        }


        /**Implementacion del metodo toString para poder mostrar como string
         * esta clase
         *
         * @return Una string representando esta clase
         *
         * */
        @Override
        public String toString()
        {
                String s = "";

                for(Jugador j: this.titulares)
                {
                        s += j.toString();
                        s += "\n";
                }

                s += "valida: " + this.valida() + "\n";
                s += "puntaje: " + this.getPuntajeTotal();
                s += "\n";

                return s;
        }
}
